package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    // private CellState cellState;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        // this.cellState = initialState;
        this.grid = new CellState[rows][columns];
        for (int i = 0; i < this.rows; i++){
            for (int j = 0; j < this.columns; j++) {
                this.grid[i][j] = initialState;
            } 
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > numRows()) {
			throw new IndexOutOfBoundsException("The row choice is invalid.");
        }
        else if (column < 0 || column > numColumns()) {
			throw new IndexOutOfBoundsException("The column choice is invalid.");
        }
        else {
            grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row > numRows()) {
			throw new IndexOutOfBoundsException("The row choice is invalid.");
        }
        else if (column < 0 || column > numColumns()) {
			throw new IndexOutOfBoundsException("The column choice is invalid.");
        }
        else {
            return grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        
        for (int i=0; i<numRows(); i++) {
            for (int j=0; j<numColumns(); j++) {
                gridCopy.set(i,j, get(i, j));
            }
        }
        return gridCopy;
    }
    
}
