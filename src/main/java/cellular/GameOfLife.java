package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Objects; //  For Object.equals()

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param rows The height of the grid of cells
	 * @param columns The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i<numberOfRows(); i++) {
			for (int j = 0; j<numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// Before tutorial 
		/*
		if (getCellState(row, col).equals(CellState.ALIVE)) {
			// En levende celle med færre enn to levende naboer dør.
			// En levende celle med mer enn tre levende naboer dør.
			if (countNeighbors(row, col, CellState.DEAD) <2 
				|| countNeighbors(row, col, CellState.ALIVE) > 3) {
				return CellState.DEAD;
			}
			// En levende celle med to eller tre levende naboer overlever.
			else { 
				return CellState.ALIVE; 
			}
		} 
		else {
			// En død celle med nøyaktig tre levende naboer blir levende.
			if (countNeighbors(row, col, CellState.DEAD) == 3) {
				return CellState.ALIVE;
			}
			else {
				return CellState.DEAD;
			}
		}
		*/

		// From turtorial
	
		int countLivingNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState nextCellState = this.getCellState(row, col);
		// Object.equals() 
		if (Objects.equals(getCellState(row, col), CellState.ALIVE)) {
			// Cell is alive
			if (countLivingNeighbors <2) {
			nextCellState = CellState.DEAD;
			}
			else if (countLivingNeighbors >=2 && countLivingNeighbors <=3) {
				nextCellState = CellState.ALIVE;

			}
			else {
				nextCellState = CellState.DEAD;
			}
		}
		
		// Cell is dead
		else {
			if (countLivingNeighbors == 3) {
				nextCellState = CellState.ALIVE;
			}
		}

		return nextCellState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param row   the x-position of the cell
	 * @param col   the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// Before watching the tutorial
		/* 
		int count = 0;

		// The studied cell is an interior cell
		if (0 < row && row < numberOfRows() && 0 < col && col < numberOfColumns()) {
			for (int i = -1; i<=1; i++) {
				for (int j = -1; j<=1; j++) {
					if (i!=0 || j!=0) {
						if (getCellState(row+i, col+j).equals(state)) {
							count = count + 1;
						}
					}
				}

			}
			return count;
		}

		// The studied cell is not an interior cell

		// The studied cell is on the top edge and is not a corner cell
		else if (row == 0 && 0 < col && col < numberOfColumns()) {
			for (int j = -1; j<=1; j++) {
				if (getCellState(row+1, col+j).equals(state)) {
					count = count + 1;
				}
			}
			if (getCellState(row, col-1).equals(state)) {
				count = count + 1;
			}
			if (getCellState(row, col+1).equals(state)) {
				count = count + 1;
			}
			return count;			
		}
		
		// The studied cell is on the left edge and is not a corner cell
		else if (0 < row && row < numberOfRows() && col == 0) {
			for (int i = -1; i<=1; i++) {
				if (getCellState(row+i, col+1).equals(state)) {
					count = count + 1;
				}
			}	
			if (getCellState(row-1, col).equals(state)) {
				count = count + 1;
			}
			if (getCellState(row+1, col).equals(state)) {
				count = count + 1;
			}
			return count;
		}
		// The studied cell is on the bottom edge and is not a corner cell
		else if (row == numberOfRows() && 0 < col && col < numberOfColumns()) {
			for (int j = -1; j<=1; j++) {
				if (getCellState(row-1, col+j).equals(state)) {
					count = count + 1;
				}
			}
			if (getCellState(row, col-1).equals(state)) {
				count = count + 1;
			}
			if (getCellState(row, col+1).equals(state)) {
				count = count + 1;
			}
			return count;
		}
	
		// The studied cell is on the right edge and is not a corner cell
		else if (0 < row && row < numberOfRows() && col == numberOfColumns()) {
			for (int i = -1; i <=1; i++) {
				if (getCellState(row+i, col-1).equals(state)) {
					count = count + 1;
				}
			}
			if (getCellState(row-1, col).equals(state)) {
				count = count + 1;
			}
			if (getCellState(row+1, col).equals(state)) {
				count = count + 1;
			}
			return count;
		}
		
		// The studied cell is the top left corner cell
		else if (row == 0 && col == 0) {
			if (getCellState(row+1, col).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row+1, col+1).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row, col+1).equals(state)) {
				count = count + 1;
			}
			return count;
		}

		// The studied cell is the bottom left corner cell
		else if (row == numberOfRows() && col == 0) {
			if (getCellState(row, col+1).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row-1, col+1).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row-1, col).equals(state)) {
				count = count + 1;
			}
			return count;
		}

		// The studied cell is the bottom right corner cell
		else if (row == numberOfRows() && col == numberOfColumns()) {
			if (getCellState(row-1, col).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row-1, col-1).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row, col-1).equals(state)) {
				count = count + 1;
			}
			return count;
		}

		// The studied cell is the top right corner cell
		else if (row == 0 && col == numberOfColumns()) {
			if (getCellState(row, col-1).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row+1, col-1).equals(state)) {
				count = count + 1;
			}
			else if (getCellState(row+1, col).equals(state)) {
				count = count + 1;
			}
			return count;
		}
		
		return count;
		*/
		
		// From tutorial
		int count = 0;
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if (i < 0 || i >= this.numberOfRows()) {
					continue;
				}
				if (j < 0|| j >= this.numberOfColumns()) {
					continue;
				}
				if (i==row && j == col) {
					continue;
				}
				if (Objects.equals(state, this.getCellState(i, j))) {
					count++;
				}
			}
		}

		return count;

	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
