package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Objects; //  For Object.equals()

public class BriansBrain implements CellAutomaton {
		/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Brians Brain Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i<numberOfRows(); i++) {
			for (int j = 0; j<numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
        CellState nextCellState = this.getCellState(row, col);
		// Object.equals(a,b) 
        
		if (Objects.equals(getCellState(row, col), CellState.ALIVE)) {
            // En levende celle blir døende
			nextCellState = CellState.DYING; 		
		}
        else if (Objects.equals(getCellState(row, col), CellState.DYING)) {
            // En døende celle blir død
            nextCellState = CellState.DEAD; 
        }
        else {
            // En død celle med akkurat 2 levende naboer blir levende
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                nextCellState = CellState.ALIVE;
            }
            else {// Not necessary to define
            // En død celle forblir død ellers
                nextCellState = CellState.DEAD; 
            }
        }
    
		return nextCellState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param row     the x-position of the cell
	 * @param col     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// From tutorial
		int count = 0;
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if (i < 0 || i >= this.numberOfRows()) {
					continue;
				}
				if (j < 0|| j >= this.numberOfColumns()) {
					continue;
				}
				if (i==row && j == col) {
					continue;
				}
				if (Objects.equals(state, this.getCellState(i, j))) {
					count++;
				}
			}
        }
        return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}

}
